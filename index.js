// import data1 from "./data1.json"
// import data2 from 2-input.js

const data = require("./data1.json");
// const data2 = require("./data2.json");

const a = data.revenueData;
const b = data.expenseData;

//sorting both arrays
a.sort((i, j) => {
  return new Date(i.startDate) - new Date(j.startDate);
});
b.sort((i, j) => {
  return new Date(i.startDate) - new Date(j.startDate);
});

// start and last date possible
const st = a[0].startDate > b[0].startDate ? b[0].startDate : a[0].startDate;
const lst =
  a[a.length - 1].startDate < b[b.length - 1].startDate
    ? b[b.length - 1].startDate
    : a[a.length - 1].startDate;

   
// GET MONTHs
let p = new Date(st);
p = p.getMonth();
q = new Date(lst).getMonth();

//finalarray initialize
const finalArray = new Array(q-p    );

for (let i = p; i <= q; i++) {
  let date = new Date(st);
  date=new Date(date.setMonth(i));
  date = date.toISOString();

  const a1 = a.filter((revenue) => {
    return revenue.startDate && revenue.startDate == date;
  });
  const b1 = b.filter((expense) => {
    return expense.startDate && expense.startDate == date;
  });

// sum of arrays  
  x=(a1.reduce((acc,cur)=>{
    return acc + cur.amount
  },0));
  y=(b1.reduce((acc,cur)=>{
    return acc + cur.amount
  },0));

  finalArray.push({
    amount : x - y,
    startDate : date
  })
}

// console.log(finalArray)

const arrayToString = JSON.stringify(Object.assign({}, finalArray));  // convert array to string
const  stringToJsonObject = JSON.parse(arrayToString);  // convert string to json object

console.log(stringToJsonObject);