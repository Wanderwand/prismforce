// importing data as json file
// const data = require("./data1.json");
// const fs= require('fs')

const FinalArray=(inputFile)=>{
// Array of expense and revene data
expenseData = inputFile.expenseData;
revenueData = inputFile.revenueData;

// function to get firstDate and lastDate dates
// sort over map as O(n)
// if a==0 it returns start date : returns lastDate date
const getFirstAndLastDate = (arr)=>{
  let min=arr[0].startDate;
  let max=arr[0].startDate
    arr.forEach((el)=> {
      min > el.startDate ? min =el.startDate : min
      max < el.startDate ? max =el.startDate : max
    });

    // return [min,max]
   return [min,max]
  }

// x is array of min and max dates of revenuData , same goes for y
  const x = getFirstAndLastDate(revenueData)
  const y = getFirstAndLastDate(expenseData)
// check which of expense or revenue startDate has firstDate and lastdate
const firstDate =  x[0] > y[0]? y[0] : x[0] 
const lastDate =  x[1] > y[1]? x[1] : y[1] 

// function to calculate total months from firstDate (if months > 12) 
const months = (date,firstDate)=>{
  let  b = new Date(date);
  let  a=new Date(firstDate);
  return (b.getMonth() -a.getMonth() + 12 * (b.getFullYear() -a.getFullYear()) )
}
// total no of months possible in our data

const m = months(lastDate,firstDate) ; 

// initialize final array with amount 0 
const finalArray = new Array();

// firstMonth (january is 0 ..) and firstYear 
const firstMonth = (new Date(firstDate)).getMonth()
// const firstYear = (new Date(firstDate)).getFullYear()

for(let i=firstMonth;i<=firstMonth+m;i++){
  // setting date with setMonth property | Here date is in miliseconds format
  const date = (new Date(firstDate)).setMonth(i);
  finalArray.push({
    amount : 0,
    startDate : new Date(date)      // converting ms to date format
  })
}
// get index 
const index=(date) =>{
  return months(date,firstDate)
}

// looping from first month to lastmonth and with  += revenueData  and -= expensData
revenueData.map(revenue=>{
    const i=index(revenue.startDate)
    finalArray[i].amount += revenue.amount
})
expenseData.map(expense=>{
  const i=index(expense.startDate)
  finalArray[i].amount -=  expense.amount
})

return finalArray
}
// 
// // convert array to string
// const arrayToString = JSON.stringify({balance : finalArray}); 
// // // // convert string to json object
// const  stringToJsonObject = JSON.parse(arrayToString);  

// // console.log(stringToJsonObject);
// fs.appendFile('./prismforce/output1.json', arrayToString, function (err) {
//   if (err) throw err;
//   console.log('Output1 generated!');
// }); 


// export default FinalArray
module.exports = FinalArray